import { initializeApp } from 'firebase';

const app = initializeApp({

	apiKey: "AIzaSyAxD9_Gg6WL5IgQuShHFTX23bKqTsVcnbs",
	authDomain: "fir-vue-ab2f4.firebaseapp.com",
	databaseURL: "https://fir-vue-ab2f4.firebaseio.com",
	projectId: "fir-vue-ab2f4",
	storageBucket: "",
	messagingSenderId: "527018815940"
});

//export firebase database
export const db = app.database();

//export list of items
export const itemsRef = db.ref('items');